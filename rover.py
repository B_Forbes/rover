# Import DroneKit-Python
from dronekit import connect, VehicleMode
import time


# Connect to the Vehicle.
print "Connecting to vehicle on: 'tcp:127.0.0.1:5760'"
vehicle = connect('/dev/ttyMFD1,57600', wait_ready=True)

print "Is Armable?: %s" % vehicle.is_armable
print "System status: %s" % vehicle.system_status.state
print "Mode: %s" % vehicle.mode.name    # settable

vehicle.mode = VehicleMode("GUIDED")
vehicle.armed = True
while not vehicle.armed:
    print " Getting ready to take off ..."
    time.sleep(1)

print "Armed: %s" % vehicle.armed    # settable
time.sleep(5)
print "Armed: %s" % vehicle.armed    # settable


# Close vehicle object before exiting script
vehicle.close()


print("Completed")